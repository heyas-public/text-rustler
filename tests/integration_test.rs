use std::{process::Command, fs::File, io::Write};

#[test]
fn test_query_cli() {
    let output = Command::new("target/debug/text_rustler_cli")
        .arg("-q")
        .arg("your-test-query")
        .output()
        .expect("Failed to execute command");

    assert!(output.status.success());
}

#[test]
fn test_file_cli() {
    // Create a temp file and write some text to it
    let mut file = File::create("temp_test_file.txt").expect("Could not create file");
    file.write_all(b"Hello, world!").expect("Could not write to file");

    let output = Command::new("target/debug/text_rustler_cli")
        .arg("-f")
        .arg("temp_test_file.txt")
        .output()
        .expect("Failed to execute command");

    // Cleanup
    std::fs::remove_file("temp_test_file.txt").expect("Could not remove temp file");

    assert!(output.status.success());
}

#[test]
fn test_index_file() {
    use std::fs::File;
    use std::io::Write;
    use std::env::temp_dir;
    use text_rustler::TextRustler;

    // Create a temp file
    let mut path = temp_dir();
    path.push("testfile.txt");
    let mut file = File::create(&path).unwrap();
    writeln!(file, "hello world").unwrap();

    // Test indexing the file
    let mut engine = TextRustler::new();
    engine.index_file(path.to_str().unwrap()).unwrap();
    assert!(engine.is_word_indexed("hello"));
    assert!(engine.is_word_indexed("world"));
    assert!(!engine.is_word_indexed("foobar"));
}
