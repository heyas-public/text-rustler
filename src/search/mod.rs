// src/search/mod.rs
use std::{
    collections::{HashMap, HashSet},
    fs::read_to_string,
    io::Result,
};
use strsim::levenshtein;

/// A basic index structure for storing words and their frequencies.
///
/// This data structure is designed to be used for a simple word
/// frequency counter. The words are stored in a HashSet, and the
/// frequency of each word is stored in a HashMap.
pub struct Index {
    words: HashSet<String>,
    frequency: HashMap<String, u32>,
}

impl Index {
    pub fn new() -> Self {
        Index {
            words: HashSet::new(),
            frequency: HashMap::new(),
        }
    }

    /// Indexes the given words and maintains the frequency of each word in the HashMap.
    pub fn index_words(&mut self, words: Vec<&str>) {
        for &word in &words {
            self.words.insert(word.to_string());
            let counter = self.frequency.entry(word.to_string()).or_insert(0);
            *counter += 1;
        }
    }

    /// Checks if the given word is indexed.
    pub fn is_word_indexed(&self, word: &str) -> bool {
        self.words.contains(word)
    }

    /// Searches for the given word in the index.
    pub fn search(&self, query: &str) -> bool {
        // Perform the search here and return the results
        self.words.contains(query)
    }

    /// Searches for the given word in index and returns the frequency of the word.
    pub fn search_with_ranking(&self, query: &str) -> Option<u32> {
        self.frequency.get(query).cloned()
    }

    /// Searches for the given word in the index and returns a list of words that contain the query.
    pub fn search_partial(&self, query: &str) -> Vec<String> {
        self.words
            .iter()
            .filter(|word| word.contains(query))
            .cloned()
            .collect()
    }

    /// Performs a fuzzy search and returns a list of words that are within a Levenshtein distance less than.
    pub fn fuzzy_search(&self, query: &str) -> Vec<String> {
        let mut results = Vec::new();
        for word in &self.words {
            if levenshtein(word, query) <= 2 {
                results.push(word.clone());
            }
        }
        results
    }

    /// Indexes the words in the given file.
    pub fn index_file(&mut self, file_path: &str) -> Result<()> {
        let text = read_to_string(file_path)?;
        let words: Vec<&str> = text.split_whitespace().collect();
        self.index_words(words);
        Ok(())
    }
}
