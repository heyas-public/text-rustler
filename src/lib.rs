// src/lib.rs

pub mod search;

pub struct TextRustler {
    search_index: search::Index,
}

impl TextRustler {
    pub fn new() -> Self {
        TextRustler {
            search_index: search::Index::new(),
        }
    }

    pub fn index_text(&mut self, text: &str) {
        let words: Vec<&str> = text.split_whitespace().collect();
        self.search_index.index_words(words);
    }

    pub fn is_word_indexed(&self, word: &str) -> bool {
        self.search_index.is_word_indexed(word)
    }

    pub fn search(&self, query: &str) -> bool {
        self.search_index.search(query)
    }

    pub fn search_with_ranking(&self, query: &str) -> Option<u32> {
        self.search_index.search_with_ranking(query)
    }

    pub fn search_partial(&self, query: &str) -> Vec<String> {
        self.search_index.search_partial(query)
    }

    pub fn fuzzy_search(&self, query: &str) -> Vec<String> {
        self.search_index.fuzzy_search(query)
    }
    
    pub fn index_file(&mut self, file_path: &str) -> std::io::Result<()> {
        self.search_index.index_file(file_path)
    }
}
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_indexing() {
        let mut engine = TextRustler::new();
        engine.index_text("hello world");
        assert!(engine.is_word_indexed("hello"));
        assert!(engine.is_word_indexed("world"));
        assert!(!engine.is_word_indexed("foobar"));
    }

    #[test]
    fn test_search() {
        let mut engine = TextRustler::new();
        engine.index_text("hello world");
        assert!(engine.search("hello"));
        assert!(engine.search("world"));
        assert!(!engine.search("foobar"));
    }

    #[test]
    fn test_search_with_ranking() {
        let mut engine = TextRustler::new();
        engine.index_text("hello hello world");
        assert_eq!(engine.search_with_ranking("hello"), Some(2));
        assert_eq!(engine.search_with_ranking("world"), Some(1));
        assert_eq!(engine.search_with_ranking("foobar"), None);
    }

    #[test]
    fn test_search_partial() {
        let mut engine = TextRustler::new();
        engine.index_text("hello world hella holla");
        assert_eq!(engine.search_partial("hello"), vec!["hello"]);
        assert_eq!(engine.search_partial("world"), vec!["world"]);
        assert_eq!(engine.search_partial("foobar"), Vec::<String>::new());
        let results = engine.search_partial("ell");
        assert!(results.contains(&"hello".to_string()));
        assert!(results.contains(&"hella".to_string()));
    }

    #[test]
    fn test_fuzzy_search() {
        let mut engine = TextRustler::new();
        engine.index_text("hello world hella holla");
        let results = engine.fuzzy_search("ell");
        assert!(results.contains(&"hello".to_string()));
        assert!(results.contains(&"hella".to_string()));
    }
}
