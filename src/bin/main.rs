extern crate clap;
use clap::{App, Arg};
use std::io::{self, Write};
use text_rustler::TextRustler;

fn main() {
    let matches = App::new("TextRustler CLI")
        .version("1.0")
        .author("You!")
        .about("Searches text like a pro")
        .arg(
            Arg::with_name("query")
                .short("q")
                .long("query")
                .value_name("QUERY")
                .help("The text query to search for")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("file")
                .short("f")
                .long("file")
                .value_name("FILE")
                .help("The text file to search in")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("repl")
                .short("r")
                .long("repl")
                .help("Activate REPL mode"),
        )
        .get_matches();

    let mut engine = TextRustler::new();

    if let Some(file_path) = matches.value_of("file") {
        let text = std::fs::read_to_string(file_path).expect("Could not read file");
        engine.index_text(&text);
    } else {
        engine.index_text("Your test data here..."); // Default data if no file provided
    }

    if matches.is_present("repl") {
        loop {
            let mut query = String::new();
            print!("search> ");
            io::stdout().flush().unwrap();
            io::stdin().read_line(&mut query).unwrap();
            let results = engine.search(&query.trim());
            println!("Results: {:?}", results);
        }
    } else if let Some(query) = matches.value_of("query") {
        let results = engine.search(query);
        println!("Results for query '{}': {:?}", query, results);
    }
}
